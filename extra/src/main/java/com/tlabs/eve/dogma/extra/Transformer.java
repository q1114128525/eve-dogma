package com.tlabs.eve.dogma.extra;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.tlabs.eve.dogma.model.Attribute;
import com.tlabs.eve.dogma.Fitting;

final class Transformer {
    private Transformer() {}

    public static final List<Fitting> fromMapper(final InputStream in, ObjectMapper jsonMapper) throws IOException {
        final ObjectReader r = jsonMapper.readerFor(XFittings.class);
        final XFittings fittings = r.without(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES).readValue(in);
        return transform(fittings);
    }

    private static List<Fitting> transform(final XFittings fittings) {
        if (null == fittings) {
            return Collections.emptyList();
        }

        final List<Fitting> returned = new ArrayList<>(fittings.getFittings().size());
        for (XFitting xf: fittings.getFittings()) {
            returned.add(transform(xf));
        }
        return returned;
    }

    private static Fitting transform(final XFitting xf) {
        final Fitting fitting = new Fitting();
        fitting.setTypeName(xf.getShipType());
        fitting.setName(xf.getName());
        fitting.setDescription(xf.getDescription());
        for (XHardware h: xf.getHardware()) {
            addHardware(fitting, h);
        }
        return fitting;
    }

    private static void addHardware(final Fitting fitting, final XHardware xh) {
        if ("drone bay".equals(xh.getSlot())) {
            fitting.addDrone(xh.getType(), xh.getQuantity());
            return;
        }
        if (StringUtils.startsWith(xh.getSlot(), "hi slot")) {
            fitting.addModule(Attribute.FIT_HIGH_SLOTS, xh.getType());
            return;
        }
        if (StringUtils.startsWith(xh.getSlot(), "med slot")) {
            fitting.addModule(Attribute.FIT_MEDIUM_SLOTS, xh.getType());
            return;
        }
        if (StringUtils.startsWith(xh.getSlot(), "low slot")) {
            fitting.addModule(Attribute.FIT_LOW_SLOTS, xh.getType());
            return;
        }
        if (StringUtils.startsWith(xh.getSlot(), "rig slot")) {
            fitting.addModule(Attribute.FIT_RIGS_SLOTS, xh.getType());
            return;
        }
        if (StringUtils.startsWith(xh.getSlot(), "subsystem")) {
            fitting.addModule(Attribute.FIT_SUBSYSTEM_SLOTS, xh.getType());
            return;
        }
    }
}
