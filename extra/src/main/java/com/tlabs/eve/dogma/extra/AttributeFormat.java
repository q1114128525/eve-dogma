package com.tlabs.eve.dogma.extra;


import java.util.HashMap;
import java.util.Map;
import com.tlabs.eve.dogma.model.Attribute;

public final class AttributeFormat {

    private interface Format {
        String format(final float attrValue);
    }

    private static final class UnitFormat implements Format {

        private final String unit;

        public UnitFormat(final String unit) {
            this.unit = unit;
        }

        @Override
        public String format(final float attrValue) {
            return attrValue + " " + unit;
        }
    }

    private static final Format defaultFormat = new Format() {
        @Override
        public String format(final float attrValue) {
            return Float.toString(attrValue);
        }
    };

    private static final Format integerFormat = new Format() {
        @Override
        public String format(final float attrValue) {
            return Integer.toString(Math.round(attrValue));
        }
    };

    private static final Format rangeFormat = new Format() {
        @Override
        public String format(final float attrValue) {
            return Integer.toString((int) (attrValue / 1000f)) + "Km";
        }
    };

    private static final Format secondFormat = new Format() {
        @Override
        public String format(final float attrValue) {
            return Integer.toString((int) (attrValue / 1000f)) + "s";
        }
    };

    private static final Format resistanceFormat = new Format() {
        @Override
        public String format(final float attrValue) {
            return Integer.toString((int) (100f - attrValue * 100f)) + "%";
        }
    };

    private static final Map<Integer, Format> attributesFormat;

    static {
        attributesFormat = new HashMap<>();
        attributesFormat.put(Attribute.FIT_HIGH_SLOTS, integerFormat);
        attributesFormat.put(Attribute.FIT_MEDIUM_SLOTS, integerFormat);
        attributesFormat.put(Attribute.FIT_LOW_SLOTS, integerFormat);
        attributesFormat.put(Attribute.FIT_RIGS_SLOTS, integerFormat);
        attributesFormat.put(Attribute.FIT_SUBSYSTEM_SLOTS, integerFormat);
        //attributesFormat.put(Attribute.FIT_UPGRADES, integerFormat);
        attributesFormat.put(Attribute.FIT_LAUNCHERS, integerFormat);
        attributesFormat.put(Attribute.FIT_TURRETS, integerFormat);
        attributesFormat.put(Attribute.FIT_DRONES, integerFormat);

        attributesFormat.put(Attribute.MASS, new UnitFormat("Kg"));
        attributesFormat.put(Attribute.VOLUME, new UnitFormat("m3"));

        attributesFormat.put(Attribute.STRUCTURE_HP, new UnitFormat("HP"));
        attributesFormat.put(Attribute.DRONE_CAPACITY, new UnitFormat("m3"));
        attributesFormat.put(Attribute.DRONE_BANDWIDTH, new UnitFormat("Mbit/sec"));

        attributesFormat.put(Attribute.STRUCTURE_INERTIA_MOD, new UnitFormat("x"));
        attributesFormat.put(Attribute.STRUCTURE_EM, resistanceFormat);
        attributesFormat.put(Attribute.STRUCTURE_EXP, resistanceFormat);
        attributesFormat.put(Attribute.STRUCTURE_KINETIC, resistanceFormat);
        attributesFormat.put(Attribute.STRUCTURE_THERMAL, resistanceFormat);

        attributesFormat.put(Attribute.ARMOR_HP, new UnitFormat("HP"));
        attributesFormat.put(Attribute.ARMOR_EM_RES, resistanceFormat);
        attributesFormat.put(Attribute.ARMOR_EXP_RES, resistanceFormat);
        attributesFormat.put(Attribute.ARMOR_KINETIC_RES, resistanceFormat);
        attributesFormat.put(Attribute.ARMOR_THERMAL_RES, resistanceFormat);

        attributesFormat.put(Attribute.SHIELD_HP, new UnitFormat("HP"));
        attributesFormat.put(Attribute.SHIELD_RECHARGE, secondFormat);
        attributesFormat.put(Attribute.SHIELD_EM_RES, resistanceFormat);
        attributesFormat.put(Attribute.SHIELD_EXP_RES, resistanceFormat);
        attributesFormat.put(Attribute.SHIELD_KINETIC_RES, resistanceFormat);
        attributesFormat.put(Attribute.SHIELD_THERMAL_RES, resistanceFormat);

        attributesFormat.put(Attribute.CAPACITOR_CAPACITY, new UnitFormat("Gj"));
        attributesFormat.put(Attribute.CAPACITOR_RECHARGE, secondFormat);

        attributesFormat.put(Attribute.TARGETING_RANGE, rangeFormat);
        attributesFormat.put(Attribute.TARGETING_TARGETS, new UnitFormat("x"));
        attributesFormat.put(Attribute.GRAVIMETRIC_STRENGTH, new UnitFormat("points"));
        attributesFormat.put(Attribute.SCAN_RESOLUTION, new UnitFormat("mm"));
        attributesFormat.put(Attribute.SIGNATURE_RADIUS, new UnitFormat("m"));

        attributesFormat.put(Attribute.VELOCITY_MAX, new UnitFormat("m/s"));
        attributesFormat.put(Attribute.VELOCITY_WARP, new UnitFormat("AU/s"));

        attributesFormat.put(Attribute.CPU_NEED, integerFormat);
        attributesFormat.put(Attribute.POWER_NEED, integerFormat);
    }

    private AttributeFormat() {
    }

    public static String format(final long attrID, final float attrValue) {
        final Format format = attributesFormat.get(Integer.valueOf((int)attrID));
        if (null == format) {
            return Float.toString(attrValue);
        }
        return format.format(attrValue);
    }

    public static String format(final Attribute attribute) {
        if (null == attribute) {
            return "0";
        }
        return format(attribute.getAttributeID(), attribute.getCurrentValue());
    }

}
