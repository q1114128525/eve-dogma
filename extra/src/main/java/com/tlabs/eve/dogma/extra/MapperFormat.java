package com.tlabs.eve.dogma.extra;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.tlabs.eve.dogma.model.Attribute;
import com.tlabs.eve.dogma.Fitting;

class MapperFormat {

    protected final ObjectMapper mapper;

    protected MapperFormat(final ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public final List<Fitting> fromContent(final InputStream in) throws IOException {
        final ObjectReader r = mapper.readerFor(XFittings.class);
        final XFittings fittings = r.without(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES).readValue(in);
        if (null == fittings) {
            return Collections.emptyList();
        }

        final List<Fitting> returned = new ArrayList<>(fittings.getFittings().size());
        for (XFitting xf: fittings.getFittings()) {
            returned.add(transform(xf));
        }
        return returned;
    }

    protected static Fitting transform(final XFitting xf) {
        final Fitting fitting = new Fitting();
        fitting.setTypeName(xf.getShipType());
        fitting.setName(xf.getName());
        fitting.setDescription(xf.getDescription());
        for (XHardware h: xf.getHardware()) {
            addHardware(fitting, h);
        }
        return fitting;
    }

    private static void addHardware(final Fitting fitting, final XHardware xh) {
        if ("drone bay".equals(xh.getSlot())) {
            fitting.addDrone(xh.getType(), xh.getQuantity());
            return;
        }
        if (StringUtils.startsWith(xh.getSlot(), "hi slot")) {
            fitting.addModule(Attribute.FIT_HIGH_SLOTS, xh.getType());
            return;
        }
        if (StringUtils.startsWith(xh.getSlot(), "med slot")) {
            fitting.addModule(Attribute.FIT_MEDIUM_SLOTS, xh.getType());
            return;
        }
        if (StringUtils.startsWith(xh.getSlot(), "low slot")) {
            fitting.addModule(Attribute.FIT_LOW_SLOTS, xh.getType());
            return;
        }
        if (StringUtils.startsWith(xh.getSlot(), "rig slot")) {
            fitting.addModule(Attribute.FIT_RIGS_SLOTS, xh.getType());
            return;
        }
        //TODO subsystems?
    }
}
