package com.tlabs.eve.dogma.extra;

import java.util.LinkedList;
import java.util.List;

class XFittings {

    private List<XFitting> fittings = new LinkedList<>();

    public List<XFitting> getFittings() {
        return fittings;
    }

    public void setFittings(List<XFitting> fittings) {
        this.fittings = fittings;
    }
}
