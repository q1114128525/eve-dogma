package com.tlabs.eve.dogma.extra;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Map;
import com.tlabs.eve.dogma.model.Attribute;
import com.tlabs.eve.dogma.Fitting;

final class ClipboardFormat {

    public static void toClipboardContent(final List<Fitting> fittings, final OutputStream out) throws IOException {
        final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
        writer.write(toClipboardContent(fittings));
        writer.flush();
    }

    public static String toClipboardContent(final List<Fitting> fittings) {
        final StringBuilder b = new StringBuilder();
        for (Fitting f: fittings) {
            b.append(toClipboardContent(f));
            b.append("\n\n");
        }
        return b.toString();
    }

    public static String toClipboardContent(final Fitting f) {
        final StringBuilder b = new StringBuilder();
        b.append("[");
        b.append(f.getTypeName());
        b.append(",");
        b.append(f.getName());
        b.append("]\n");

        b.append(toClipboard(f, Attribute.FIT_LOW_SLOTS));
        b.append(toClipboard(f, Attribute.FIT_MEDIUM_SLOTS));
        b.append(toClipboard(f, Attribute.FIT_HIGH_SLOTS));
        b.append(toClipboard(f, Attribute.FIT_RIGS_SLOTS));
        b.append(toClipboard(f, Attribute.FIT_SUBSYSTEM_SLOTS));

        b.append(toClipboard(f.getDrones()));
        b.append(toClipboard(f.getCargo()));

        return b.toString();
    }

    private static String toClipboard(final Fitting f, final int slotAttributeId) {
        final List<String> modules = f.getModules(slotAttributeId);
        if ((null == modules) || (modules.isEmpty())) {
            return "";
        }
        final StringBuilder b = new StringBuilder();
        for (String m: modules) {
            b.append(m);
            b.append("\n");
        }
        b.append("\n\n");
        return b.toString();
    }

    private static String toClipboard(Map<String, Integer> items) {
        if (items.isEmpty()) {
            return "";
        }
        StringBuilder b = new StringBuilder();
        for (Map.Entry<String, Integer> e: items.entrySet()) {
            b.append(e.getKey());
            b.append(" x");
            b.append(e.getValue());
        }
        b.append("\n\n");
        return b.toString();
    }
}
