package com.tlabs.eve.dogma.extra;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import com.tlabs.eve.dogma.Fitting;

public class TestXMLContent {

    @Test
    public void testFromXContent() throws Exception {
        final List<Fitting> fittings = FittingFormat.fromXML(TestXMLContent.class.getResourceAsStream("/fittings-test.xml"));
        Assert.assertNotNull("Null fittings", fittings);
        Assert.assertEquals(3, fittings.size());
    }

    @Test
    public void testToXContent() throws Exception {
        final List<Fitting> fittings = FittingFormat.fromXML(TestXMLContent.class.getResourceAsStream("/fittings-test.xml"));
        Assert.assertNotNull("Null fittings", fittings);
        Assert.assertEquals(3, fittings.size());

        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        FittingFormat.toXML(fittings, bos);
        bos.flush();

        final List<Fitting> saved = FittingFormat.fromXML(new ByteArrayInputStream(bos.toByteArray()));
        Assert.assertNotNull("Null saved fitting", saved);
        Assert.assertEquals(3, saved.size());
    }
}
