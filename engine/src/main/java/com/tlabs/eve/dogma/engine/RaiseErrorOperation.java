package com.tlabs.eve.dogma.engine;

import com.tlabs.eve.dogma.model.Expression;

final class RaiseErrorOperation extends AbstractFittingOperation {

    public RaiseErrorOperation(FittingEngine engine) {
        super(engine);
    }

    @Override
    Expression.Result apply(Expression expr, FittingModel model) {
       // throw new IllegalStateException(expr.getName());
        LOG.warn("RaiseErrorOperation not implemented");
        return new Expression.Result(1);//TODO
    }
}
