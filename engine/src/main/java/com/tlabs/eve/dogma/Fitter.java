package com.tlabs.eve.dogma;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.Validate;
import com.tlabs.eve.dogma.engine.FittingEngine;
import com.tlabs.eve.dogma.engine.FittingModel;
import com.tlabs.eve.dogma.model.Attribute;
import com.tlabs.eve.dogma.model.Effect;
import com.tlabs.eve.dogma.model.Item;

public final class Fitter {

    public static class Builder {
        private FittingProvider provider;

        public Builder(final FittingProvider provider) {
            this.provider = provider;
        }

        public Fitter build(final Fitting fitting) {
            Validate.notNull(fitting);

            final Item item = this.provider.findItem(fitting.getShipTypeID());
            Validate.notNull(item, "Invalid item '" + fitting.getShipTypeID() + "'");

            Fitter fitter = new Fitter(this.provider, item);
            for (List<String> modules: fitting.getModules().values()) {
                for (String m: modules) {
                    fitter.fit(m);
                }
            }
            //TODO cargo and drones
            return fitter;
        }

        public Fitter build(final long ship) {
            final Item item = this.provider.findItem(ship);
            Validate.notNull(item, "Invalid item '" + ship + "'");
            return new Fitter(this.provider, item);
        }

        public Fitter build(final String ship) {
            final Item item = this.provider.findItem(ship);
            Validate.notNull(item, "Invalid item '" + ship + "'");
            return new Fitter(this.provider, item);
        }
    }

    private static final FittingEngine engine = new FittingEngine();

    private final FittingModel.Ship ship;
    private final Map<Integer, List<Item>> modules;//slot attr id -> modules
    private final Map<Integer, Integer> slots;//slot attr id -> count

    private final Map<Item, Integer> cargo;
    private final Map<Item, Integer> drones;

    private String name;
    private String description;

    private final FittingProvider provider;

    private Fitter(final FittingProvider provider, final Item ship) {
        Validate.notNull(ship);
        Validate.notNull(provider);

        this.provider = provider;
        this.ship = new FittingModel.Ship(ship);
        this.slots = new HashMap<>();
        this.modules = new HashMap<>();
        this.cargo = new HashMap<>();
        this.drones = new HashMap<>();
        this.name = ship.getItemName();
        this.description = ship.getDescription();

        addSlot(Attribute.FIT_HIGH_SLOTS);
        addSlot(Attribute.FIT_MEDIUM_SLOTS);
        addSlot(Attribute.FIT_LOW_SLOTS);
        addSlot(Attribute.FIT_RIGS_SLOTS);
        addSlot(Attribute.FIT_SUBSYSTEM_SLOTS);
    }

    public Fitter(Fitter fitter) {
        this(fitter.provider, fitter.ship.getItem());
        this.slots.putAll(fitter.slots);
        this.modules.putAll(fitter.modules);
        this.cargo.putAll(fitter.cargo);
        this.drones.putAll(fitter.drones);
        this.name = fitter.name;
        this.description = fitter.description;

        for (List<Item> items: this.modules.values()) {
            for (Item m: items) {
                engine.fit(this.ship, m);
            }
        }
    }

    public Fitting toFitting() {
        final Fitting fitting = new Fitting();
        fitting.setName(this.name);
        fitting.setDescription(this.description);
        fitting.setShipTypeID(this.ship.getItem().getItemID());
        fitting.setTypeName(this.ship.getItem().getItemName());

        for (Map.Entry<Integer, List<Item>> e: this.modules.entrySet()) {
            for (Item m: e.getValue()) {
                fitting.addModule(e.getKey(), m.getItemName());
            }
        }
        for (Map.Entry<Item, Integer> e: this.cargo.entrySet()) {
            fitting.addCargo(e.getKey().getItemName(), e.getValue());
        }
        for (Map.Entry<Item, Integer> e: this.drones.entrySet()) {
            fitting.addDrone(e.getKey().getItemName(), e.getValue());
        }
        return fitting;
    }

    public String getTypeName() {
        return this.ship.getItem().getItemName();
    }

    public String getName() {
        return name;
    }

    public Fitter setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Fitter setDescription(String description) {
        this.description = description;
        return this;
    }

    public double getAttributeValue(final String attributeName) {
        final Double d = this.ship.getAttributeValue(attributeName);
        return (null == d) ? 0d : d.doubleValue();
    }

    public double getAttributeValue(final long attributeID) {
        final Double d = this.ship.getAttributeValue(attributeID);
        return (null == d) ? 0d : d.doubleValue();
    }

    public Attribute getAttribute(final String attributeName) {
        return this.ship.getAttribute(attributeName);
    }

    public Attribute getAttribute(final long attributeID) {
        return this.ship.getAttribute(attributeID);
    }

    public Map<Item, Integer> getDrones() {
        return drones;
    }

    public Map<Item, Integer> getCargo() {
        return cargo;
    }

    public int getSlotCount() {
        return this.slots.size();
    }

    public int getSlotCount(final int attributeID) {
        final Integer count = this.slots.get(attributeID);
        return (null == count) ? 0 : count.intValue();
    }

    public Map<Integer, Integer> getSlots() {
        return Collections.unmodifiableMap(this.slots);
    }

    public int getModuleCount(final int slotId) {
        final List<Item> modules = this.modules.get(slotId);
        return (null == modules) ? 0 : modules.size();
    }

    public int getModuleCount() {
        int count = 0;
        for (List<Item> l: this.modules.values()) {
            count = count + l.size();
        }
        return count;
    }

    public List<Item> getModules(final int slotId) {
        return this.modules.get(slotId);
    }

    public Item getModuleAt(final int slotId, final int position) {
        final List<Item> modules = this.modules.get(slotId);
        return (null == modules) ? null : modules.get(position);
    }

    public boolean getFull() {
        for (List<Item> l: this.modules.values()) {
            for (Item m: l) {
                if (m.getItemID() == -1) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean getFull(final int slotId) {
        final List<Item> slot = this.modules.get(slotId);
        if (null == slot) {
            return true;
        }
        for (Item m: slot) {
            if (m.getItemID() == -1) {
                return false;
            }
        }
        return true;
    }

    public Item fit(final String module) {
        final Item item = this.provider.findItem(module);
        if (fit(item)) {
            return item;
        }
        return null;
    }

    public Item fit(final long moduleID) {
        final Item item = this.provider.findItem(moduleID);
        if (fit(item)) {
            return item;
        }
        return null;
    }

    public boolean fit(final Item module) {
        if (null == module) {
            return false;
        }
        if (addModuleImpl(module)) {
            engine.fit(this.ship, module);
            return true;
        }
        return false;
    }

    public boolean unfit(final String module) {
        return unfit(this.provider.findItem(module));
    }

    public boolean unfit(final Item module) {
        if (null == module) {
            return false;
        }
        return unfit(module.getItemID());
    }

    public void unfitAll(final String module) {
        unfitAll(this.provider.findItem(module));
    }

    public void unfitAll(final Item module) {
        if (null == module) {
            return;
        }
        unfitAll(module.getItemID());
    }

    public void unfitAll(final long moduleID) {
        while (unfit(moduleID));
    }

    public boolean unfit(final long moduleID) {
        for (List<Item> items: this.modules.values()) {
            if (removeImpl(moduleID, items)) {
                return true;
            }
        }
        return false;
    }

    private boolean removeImpl(final long moduleID, final List<Item> from) {
        Item item = null;
        for (Item f: from) {
            if (f.getItemID() == moduleID) {
                item = f;
                break;
            }
        }
        if (null == item) {
            return false;
        }
        from.remove(item);
        engine.unfit(this.ship, item);
        return true;
    }

    private boolean addModuleImpl(final Item module) {
        if (addModuleImpl(module, Attribute.FIT_HIGH_SLOTS, Attribute.USES_HIGH_SLOT)) {
            return true;
        }
        if (addModuleImpl(module, Attribute.FIT_MEDIUM_SLOTS, Attribute.USES_MEDIUM_SLOT)) {
            return true;
        }
        if (addModuleImpl(module, Attribute.FIT_LOW_SLOTS, Attribute.USES_LOW_SLOT)) {
            return true;
        }
        if (addModuleImpl(module, Attribute.FIT_RIGS_SLOTS, Attribute.USES_RIG_SLOT)) {
            return true;
        }
        return addModuleImpl(module, Attribute.FIT_SUBSYSTEM_SLOTS, Attribute.USES_SUBSYSTEM);
    }

    private boolean addModuleImpl(final Item module, final int slotAttributeId, final int effectId) {
        final Effect effect = module.getEffect(effectId);
        if (null == effect) {
            return false;
        }

        List<Item> modules = this.modules.get(slotAttributeId);
        if (null == modules) {
            modules = new LinkedList<>();
            this.modules.put(slotAttributeId, modules);
        }
        modules.add(module);
        return true;
    }

    private void addSlot(int attributeId) {
        final Attribute attr = getAttribute(attributeId);
        if ((null == attr) || (attr.getCurrentValue() == 0f)) {
            return;
        }
        this.slots.put(attributeId, (int) attr.getCurrentValue());
    }
}
