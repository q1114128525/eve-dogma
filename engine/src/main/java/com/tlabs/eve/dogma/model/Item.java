package com.tlabs.eve.dogma.model;

import java.util.List;
import java.util.Map;
import com.tlabs.eve.dogma.model.Attribute;
import com.tlabs.eve.dogma.model.Effect;

public class Item {

    private long itemID;
    private String itemName;
    private String description;

    private long groupID;

    private Map<Long, Attribute> attributes;
    private List<Effect> effects;

    public long getItemID() {
        return itemID;
    }

    public void setItemID(long itemID) {
        this.itemID = itemID;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getGroupID() {
        return groupID;
    }

    public void setGroupID(long groupID) {
        this.groupID = groupID;
    }

    public Map<Long, Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<Long, Attribute> attributes) {
        this.attributes = attributes;
    }

    public List<Effect> getEffects() {
        return effects;
    }

    public Effect getEffect(final long effectID) {
        for (Effect e: this.effects) {
            if (e.getEffectID() == effectID) {
                return e;
            }
        }
        return null;
    }

    public void setEffects(List<Effect> effects) {
        this.effects = effects;
    }

    public Attribute getAttribute(final long attributeID) {
        return this.attributes.get(attributeID);
    }
    public Attribute getAttribute(final String attributeName) {
        for (Map.Entry<Long, Attribute> e: this.attributes.entrySet()) {
            if (e.getValue().getAttributeName().equals(attributeName)) {
                return e.getValue();
            }
        }
        return null;
    }

}
