package com.tlabs.eve.dogma.ormlite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "dgmEffects")
public class EffectEntity {

    @Id
    @Column
    private long effectID;

    @Column
    private String effectName;

    @Column
    private String displayName;

    @Column
    private String description;

    @Column
    private long preExpression;

    @Column
    private long postExpression;

    public long getEffectID() {
        return effectID;
    }

    public String getEffectName() {
        return effectName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getDescription() {
        return description;
    }

    public long getPreExpression() {
        return preExpression;
    }

    public long getPostExpression() {
        return postExpression;
    }
}
