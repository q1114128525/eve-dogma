package com.tlabs.eve.dogma.ormlite;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.tlabs.eve.dogma.model.Attribute;
import com.tlabs.eve.dogma.model.Effect;
import com.tlabs.eve.dogma.model.Expression;
import com.tlabs.eve.dogma.model.Item;
import com.tlabs.eve.dogma.FittingProvider;
import com.tlabs.eve.dogma.model.Operand;

public class OrmLiteFittingProvider implements FittingProvider {

    private final ConnectionSource connection;

    private final Dao<EffectEntity, Long> effectDAO;
    private final Dao<ExpressionEntity, Long> expressionDAO;
    private final Dao<AttributeEntity, Long> attributeDAO;
    private final Dao<OperandEntity, Long> operandDAO;
    private final Dao<TypeEntity, Long> itemDAO;

    public OrmLiteFittingProvider() throws SQLException {
        this("jdbc:sqlite:" + OrmLiteFittingProvider.class.getResource("/sdelite.sqlite").getFile());
    }

    public OrmLiteFittingProvider(final ConnectionSource source) throws SQLException {
        this.connection = source;
        this.effectDAO = DaoManager.createDao(this.connection, EffectEntity.class);
        this.attributeDAO = DaoManager.createDao(this.connection, AttributeEntity.class);
        this.expressionDAO = DaoManager.createDao(this.connection, ExpressionEntity.class);
        this.operandDAO = DaoManager.createDao(this.connection, OperandEntity.class);
        this.itemDAO = DaoManager.createDao(this.connection, TypeEntity.class);

        this.init();
    }

    public OrmLiteFittingProvider(final String jdbcUrl) throws SQLException {
        this(new JdbcPooledConnectionSource(jdbcUrl));
    }

    @Override
    public Item findItem(long itemID) {
        try {
            final TypeEntity entity = this.itemDAO.queryForId(itemID);
            return (null == entity) ? null : transform(entity);
        }
        catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public Item findItem(String typeName) {
        try {
            final List<TypeEntity> entities = this.itemDAO.queryForEq("typeName", StringUtils.replace(typeName, "'", "''"));
            if (CollectionUtils.isEmpty(entities)) {
                return null;
            }
            return transform(entities.get(0));
        }
        catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private void init() {
        try {
            OperandEntity op = this.operandDAO.queryForId(5l);
            if (null == op) {
                throw new SQLException();
            }
            return;
        }
        catch (SQLException e) {
            //table does not exists or has troubles
        }

        try {
            TableUtils.dropTable(this.connection, OperandEntity.class, true);
            TableUtils.createTable(this.connection, OperandEntity.class);
            TableUtils.clearTable(this.connection, OperandEntity.class);

            OperandEntity op = this.operandDAO.queryForId(5l);
            if (null == op) {
                for (OperandEntity o: loadOperandsJSON()) {
                    this.operandDAO.create(o);
                }
            }
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    private Effect findEffect(long effectID) {
        try {
            final EffectEntity e = effectDAO.queryForId(effectID);
            return (null == e) ? null : transform(e);
        }
        catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private List<Effect> findEffects(long itemID) {
        try {
            List<String[]> ids =
                    effectDAO.queryRaw("select effectID from dgmTypeEffects where typeID = ?", Long.toString(itemID)).getResults();

            final List<Effect> effects = new LinkedList<>();
            for (String[] r: ids) {
                effects.add(findEffect(Long.parseLong(r[0])));
            }
            return effects;
        }
        catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private Map<Long, Attribute> findAttributes(long itemID) {
        try {
            List<String[]> ids =
                    effectDAO.queryRaw("select attributeID, valueFloat, valueInt from dgmTypeAttributes where typeID = ?", Long.toString(itemID)).
                    getResults();

            final Map<Long, Attribute> attributes = new HashMap<>();
            for (String[] r: ids) {
                final Attribute attribute = findAttribute(Long.parseLong(r[0]));
                float value = attribute.getDefaultValue();

                if (StringUtils.isNotBlank(r[1])) {
                    value = Float.parseFloat(r[1]);
                }
                else if (StringUtils.isNotBlank(r[2])) {
                    value = Float.parseFloat(r[2]);
                }
                attribute.setCurrentValue(value);
                attributes.put(attribute.getAttributeID(), attribute);
            }
            return attributes;
        }
        catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private Attribute findAttribute(final long attributeID) {
        try {
            final AttributeEntity e = attributeDAO.queryForId(attributeID);
            return (null == e) ? null : transform(e);
        }
        catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private Expression findExpression(final long expressionID) {
        try {
            final ExpressionEntity e = expressionDAO.queryForId(expressionID);
            return (null == e) ? null : transform(e);
        }
        catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private Operand findOperand(final long operandID) {
        try {
            final OperandEntity e = operandDAO.queryForId(operandID);
            return (null == e) ? null : transform(e);
        }
        catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private Effect transform(final EffectEntity e) {
        final Effect effect = new Effect();
        effect.setEffectID(e.getEffectID());
        effect.setEffectName(e.getEffectName());
        effect.setDisplayName(e.getDisplayName());
        effect.setDescription(e.getDescription());
        effect.setPostExpression(findExpression(e.getPostExpression()));
        effect.setPreExpression(findExpression(e.getPreExpression()));
        return effect;
    }

    private Expression transform(final ExpressionEntity e) {
        final Expression expression = new Expression();
        expression.setExpressionID(e.getExpressionID());
        expression.setName(e.getExpressionName());
        expression.setDescription(e.getDescription());
        expression.setValue(e.getExpressionValue());

        expression.setAttribute(findAttribute(e.getExpressionAttributeID()));
        expression.setOperand(findOperand(e.getOperandID()));
        expression.setArg1(findExpression(e.getArg1()));
        expression.setArg2(findExpression(e.getArg2()));
        return expression;
    }

    private Attribute transform(final AttributeEntity e) {
        final Attribute attribute = new Attribute();
        attribute.setAttributeID(e.getAttributeID());
        attribute.setAttributeName(e.getAttributeName());
        attribute.setCategoryID(e.getCategoryID());
        attribute.setUnitID(e.getUnitID());
        attribute.setDefaultValue(e.getDefaultValue());
        attribute.setDescription(e.getDescription());
        attribute.setHighIsGood(e.getHighIsGood());
        attribute.setStackable(e.getStackable());

        return attribute;
    }

    private Operand transform(final OperandEntity e) {
        final Operand operand = new Operand();
        operand.setDescription(e.getDescription());
        operand.setArg1CategoryID(e.getArg1CategoryID());
        operand.setArg2CategoryID(e.getArg2CategoryID());
        operand.setFormat(e.getFormat());
        operand.setOperandID(e.getOperandID());
        operand.setOperandKey(e.getOperandKey());
        operand.setResultCategoryID(e.getResultCategoryID());
        return operand;
    }

    private Item transform(final TypeEntity e) {
        final Item item = new Item();
        item.setItemID(e.getTypeID());
        item.setGroupID(e.getGroupID());
        item.setItemName(e.getTypeName());
        item.setDescription(e.getDescription());
        item.setAttributes(findAttributes(e.getTypeID()));
        item.setEffects(findEffects(e.getTypeID()));
        return item;
    }

    private static List<OperandEntity> loadOperandsJSON() throws IOException {
        InputStream in = OrmLiteFittingProvider.class.getResourceAsStream("/dgmOperands.json");
        if (null == in) {
            throw new IOException("/dgmOperands.json  not found");
        }
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final Map<Long, OperandEntity> operands = mapper.readValue(in, new TypeReference<Map<String, OperandEntity>>() {});
            final List<OperandEntity> returned = new ArrayList<>(operands.size());
            returned.addAll(operands.values());
            return returned;
        }
        finally {
            IOUtils.closeQuietly(in);
        }
    }

}
