package com.tlabs.eve.dogma.ormlite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "invTypes")
public class TypeEntity {

    @Id
    @Column
    private long typeID;

    @Column(unique = true)
    private String typeName;

    @Column
    private long groupID;

    @Column
    private String description;

    public long getTypeID() {
        return typeID;
    }

    public long getGroupID() {
        return groupID;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getDescription() {
        return description;
    }

}
